package com.uoc.accounts.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCheckToken {
    private Integer id;

    private String uuid;

    private String username;

    private String firstName;

    private String phoneNumber;

    private String email;

    private Integer status;

    private List<String> permission;
}
