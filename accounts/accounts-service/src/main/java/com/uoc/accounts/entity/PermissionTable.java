package com.uoc.accounts.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "PERMISSION_TABLE")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermissionTable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @Column(name = "NAME")
    private String name;
}
