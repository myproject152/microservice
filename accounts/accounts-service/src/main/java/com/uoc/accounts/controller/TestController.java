package com.uoc.accounts.controller;

import com.uoc.accounts.domain.dto.UserTableDTO;
import com.uoc.accounts.service.IUserTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/test")
public class TestController {
    @Autowired
    IUserTableService service;

    @PostMapping("/register")
//    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionAdd('user'))")
    public ResponseEntity create(@RequestBody UserTableDTO userDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.create(userDTO));
    }
}
