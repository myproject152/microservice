package com.uoc.accounts.service.impl;

import com.uoc.accounts.config.exceptions.AppException;
import com.uoc.accounts.config.exceptions.ErrorResponseBase;
import com.uoc.accounts.config.security.model.SysUserDetails;
import com.uoc.accounts.constants.STATUS;
import com.uoc.accounts.domain.dto.UserTableDTO;
import com.uoc.accounts.entity.GroupTable;
import com.uoc.accounts.entity.PermissionTable;
import com.uoc.accounts.entity.RoleTable;
import com.uoc.accounts.entity.UserTable;
import com.uoc.accounts.repository.UserTableRepository;
import com.uoc.accounts.service.IEmailService;
import com.uoc.accounts.service.IPermissionService;
import com.uoc.accounts.service.IUserTableService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class UserTableService implements IUserTableService {

    @Autowired
    private UserTableRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IPermissionService permissionService;

    @Autowired
    private IEmailService emailService;

    @Autowired
    public JavaMailSender emailSender;

    @Override
    public UserTable create(UserTableDTO userDTO) {
        UserTable userTable = new UserTable();
        BeanUtils.copyProperties(userDTO, userTable);
        userTable.setCreateUserId(1);
        userTable.setCreateDate(LocalDate.now());
        userTable.setUuid(UUID.randomUUID().toString());
        userTable.setModifiedUserId(1);
        userTable.setModifiedDate(LocalDate.now());
        userTable.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userTable.setStatus(STATUS.PENDING);
        userTable = userRepository.save(userTable);
//        sendConfirmUserRegistrationViaEmail(userDTO.getEmail());

        try {
            return userTable;
        } catch (Exception e){
            throw new AppException(e);
        }
    }

    public void sendConfirmUserRegistrationViaEmail(String email) {
        try {
//            eventPublisher.publishEvent(new OnSendRegistrationUserConfirmViaEmailEvent(email));
            emailService.sendRegistrationUserConfirm(email);
        } catch (Exception e){
            throw new AppException(e);
        }
    }

    @Override
    public UserTable update(int id, UserTableDTO userDTO) {
        UserTable userTable = findById(id);
        BeanUtils.copyProperties(userDTO, userTable);
        userTable.setPassword(passwordEncoder.encode(userTable.getPassword()));
        try {
            return userRepository.save(userTable);
        } catch (Exception e){
            throw new AppException(e);
        }
    }

    public UserTable findById(int id) {
        if (userRepository.findById(id).isEmpty()){
            throw new AppException(ErrorResponseBase.NOT_FOUND);
        }
        return userRepository.findById(id).get();
    }

    public List<UserTable> getAll() {
        return userRepository.findAll();
    }

    @Override
    public UserTable addPermission(int id, int permission) {
        Set<PermissionTable> permissionTables = new HashSet<>();
//        int[] permissions = {1,2,3,4};
//        for (int permission: permissions) {
//            permissionTables.add(permissionService.getById(permission));
//        }
        UserTable userTable = findById(id);
        permissionTables = userTable.getPermissions();
        permissionTables.add(permissionService.getById(permission));

        userTable.setPermissions(permissionTables);
        return userRepository.save(userTable);
    }

    @Override
    public UserTable findByEmail(String email) {
        if (userRepository.findByEmail(email).isEmpty()){
            throw new AppException(ErrorResponseBase.NOT_FOUND);
        }
        return userRepository.findByEmail(email).get();
    }

    @Override
    public void activeUser(String token) {
        if (userRepository.findByUuid(token).isEmpty()){
            throw new AppException(ErrorResponseBase.NOT_EXISTED);
        }
        UserTable userTable = userRepository.findByUuid(token).get();
        userTable.setStatus(STATUS.ACTIVE);
        userRepository.save(userTable);
    }

    @Override
    public UserDetails loadUserByUsername(String username){
        Optional<UserTable> user = userRepository.findByUsername(username);
        SysUserDetails sysUserDetails = new SysUserDetails();
        Set<GrantedAuthority> permissions = new HashSet<>();

        if (user.isPresent()) {
            BeanUtils.copyProperties(user.get(), sysUserDetails);

            for (PermissionTable permission : sysUserDetails.getPermissions()) {
                permissions.add(new SimpleGrantedAuthority(permission.getName()));
            }

            for (RoleTable role : sysUserDetails.getRoles()) {
                for (PermissionTable permission : role.getPermissions()) {
                    permissions.add(new SimpleGrantedAuthority(permission.getName()));
                }
            }

            for (GroupTable group : sysUserDetails.getGroups()) {
                for (PermissionTable permission : group.getPermissions()) {
                    permissions.add(new SimpleGrantedAuthority(permission.getName()));
                }
            }
            sysUserDetails.setAuthorities(permissions);
            return sysUserDetails;
        } else {
            return null;
//            throw new AppException(ErrorResponseBase.USER_NOT_FOUND);
        }
    }
}
