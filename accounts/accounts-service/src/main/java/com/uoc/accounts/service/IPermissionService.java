package com.uoc.accounts.service;

import com.uoc.accounts.entity.PermissionTable;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IPermissionService {
    PermissionTable getById(int id);
    PermissionTable save(String name);
    List<PermissionTable> getAll();
}
