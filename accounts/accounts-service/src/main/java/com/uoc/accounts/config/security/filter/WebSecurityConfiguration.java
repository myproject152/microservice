package com.uoc.accounts.config.security.filter;

import com.uoc.accounts.config.security.handler.JwtAuthenticationEntryPoint;
import com.uoc.accounts.config.security.handler.UserLoginFailureHandler;
import com.uoc.accounts.config.security.handler.UserLoginSuccessHandler;
import com.uoc.accounts.config.security.token.JWTConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint userNotLoginHandler;

    @Autowired
    private UserLoginSuccessHandler userLoginSuccessHandler;

    @Autowired
    private UserLoginFailureHandler userLoginFailureHandler;

    @Autowired
    private UserAuthenticationProvider userAuthenticationProvider;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

//    @Bean
//    public DefaultWebSecurityExpressionHandler userSecurityExpressionHandler(UserPermissionEvaluator userPermissionEvaluator) {
//        DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();
//        handler.setPermissionEvaluator(userPermissionEvaluator);
//        return handler;
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(userAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        Quan trọng
        http.authorizeRequests()
                .antMatchers(JWTConfig.antMatchers.split(",")).permitAll()
                .antMatchers(HttpMethod.POST,"/api/accounts/user").hasAnyAuthority("admin", "user")
                .anyRequest().permitAll()
//  ------------------------

                .and().httpBasic().authenticationEntryPoint(userNotLoginHandler)
                .and().formLogin().loginProcessingUrl("/api/accounts/login")
                .successHandler(userLoginSuccessHandler)
                .failureHandler(userLoginFailureHandler)


                .and().cors()
                .and().csrf().disable();



        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.headers().cacheControl();
//        http.addFilter(new JwtRequestFilter(authenticationManager()));
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

    }

}
