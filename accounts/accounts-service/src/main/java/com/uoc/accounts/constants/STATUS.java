package com.uoc.accounts.constants;

public enum STATUS {
    ACTIVE(1, "active"),
    PENDING(2,"pending"),
    DE_ACTIVE(3,"de-active"),
    DELETE(4,"delete");

    public final int status;
    public final String display;

    STATUS(int status, String display) {
        this.status = status;
        this.display = display;
    }

    public int getStatus() {
        return status;
    }

    public String getDisplay() {
        return display;
    }
}
