package com.uoc.accounts.config.security.token;

import com.alibaba.fastjson.JSON;
import com.uoc.accounts.config.security.exeption.ResponseAuthentication;
import com.uoc.accounts.config.security.model.SysUserDetails;
import com.uoc.accounts.config.security.model.Token;
import com.uoc.accounts.service.IUserTableService;
import com.uoc.accounts.service.JWTTokenService;
import com.uoc.accounts.service.impl.UserTableService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Component
public class JWTTokenUtils {
    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private static JWTTokenService jwtTokenService;

    @Autowired
    private JWTTokenService jwtTokenServiceAutowired;

    @PostConstruct
    public void init() {
        jwtTokenService = this.jwtTokenServiceAutowired;
    }

    public static String createAccessToken(SysUserDetails sysUserDetails) {
        String token = Jwts.builder()
                .setId(sysUserDetails.getId().toString())
                .setSubject(sysUserDetails.getUsername())
                .setIssuedAt(new Date())
                .setIssuer("NMS")
                .setExpiration(new Date(System.currentTimeMillis() + JWTConfig.expiration))
                .signWith(Keys.hmacShaKeyFor(JWTConfig.secret.getBytes()))
                .claim("authorities", JSON.toJSONString(sysUserDetails.getAuthorities()))
                .claim("ip", sysUserDetails.getIp()).compact();
        return JWTConfig.tokenPrefix + token;
    }

//    public static String refreshAccessToken(String oldToken, StringRedisTemplate redisTemplate) {
//        String username = JWTTokenUtils.getUserNameByToken(oldToken, redisTemplate);
//        SysUserDetails sysUserDetails = (SysUserDetails) jwtTokenUtils.jwtUserDetailService
//                .loadUserByUsername(username);
//        sysUserDetails.setIp(JWTTokenUtils.getIpByToken(oldToken, redisTemplate));
//        return createAccessToken(sysUserDetails);
//    }

    public static SysUserDetails parseAccessToken(IUserTableService service, String token) {
//        SysUserDetails sysUserDetails = new SysUserDetails();
        if (!token.isEmpty()) {
            try {
                token = token.substring(JWTConfig.tokenPrefix.length()).trim();
                Claims claims = Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(JWTConfig.secret.getBytes())).build().parseClaimsJws(token).getBody();
                SysUserDetails sysUserDetails = (SysUserDetails) service.loadUserByUsername(claims.getSubject());
                sysUserDetails.setId(Integer.valueOf(claims.getId()));

                String ip = claims.get("ip").toString();
                sysUserDetails.setIp(ip);
                return sysUserDetails;
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return null;
    }

    public static void setTokenInfo(String token, String username, String userAgent) {
        if (!token.isEmpty()) {
            token = token.replace(JWTConfig.tokenPrefix, "").trim();
            LocalDateTime localDateTime = LocalDateTime.now();
            LocalDateTime refreshTime = localDateTime.plus(JWTConfig.refreshTime, ChronoUnit.MILLIS);
            LocalDateTime expiration = localDateTime.plus(JWTConfig.refreshTime, ChronoUnit.MILLIS);
            String ip = userAgent;
            boolean isBlackList = false;
            Token entity = new Token(token, ip, isBlackList, refreshTime, expiration);
            jwtTokenService.save(entity);
        }
    }

    public static void updateExpiration(String token) {
        if (!token.isEmpty()) {
            Token entity = jwtTokenService.findByToken(token);
            token = token.replace(JWTConfig.tokenPrefix, "").trim();

            Integer refreshTime = JWTConfig.refreshTime;
            LocalDateTime localDateTime = LocalDateTime.now();

            entity.setExpiration(localDateTime.plus(JWTConfig.expiration, ChronoUnit.MILLIS));
            entity.setRefreshTime(localDateTime.plus(JWTConfig.refreshTime, ChronoUnit.MILLIS));
            entity.setRefreshTime(localDateTime.plus(JWTConfig.refreshTime, ChronoUnit.MILLIS));

            jwtTokenService.save(entity);
        }
    }

    public static boolean checkToken(String token, HttpServletResponse response, HttpServletRequest httpServletRequest) {
        try {
            Token entity = jwtTokenService.findByToken(token);
            if (token == null || !token.startsWith(JWTConfig.tokenPrefix)) {
                ResponseAuthentication.responseJson(response, new ResponseAuthentication(401, "Token ko hợp lệ", httpServletRequest.getRequestURI()));
                return false;
            }
            token = token.replace(JWTConfig.tokenPrefix, "").trim();
            if (entity == null) {
                ResponseAuthentication.responseJson(response, new ResponseAuthentication(401, "Token ko tồn tại", httpServletRequest.getRequestURI()));
                return false;
            }

            if (isBlackList(token, entity)) {
                ResponseAuthentication.responseJson(response, new ResponseAuthentication(401, "Token trong danh sách đen", httpServletRequest.getRequestURI()));
                return false;
            }

            String ip = httpServletRequest.getHeader("User-Agent");
            String ip2 = httpServletRequest.getHeader("UserAgent");
            if (!Objects.equals(ip, entity.getIp()) && !Objects.equals(ip2, entity.getIp())) {
                ResponseAuthentication.responseJson(response, new ResponseAuthentication(401, "Token đc dùng ở chỗ khác", httpServletRequest.getRequestURI()));
                return false;
            }
//            if (JWTTokenUtils.isExpiration(entity.getExpiration().toString())) {
//                ResponseAuthentication.responseJson(response, new ResponseAuthentication(401, "Token hết hiệu lực", httpServletRequest.getRequestURI()));
//                return false;
//            }
        } catch (Exception e) {
            ResponseAuthentication.responseJson(response, new ResponseAuthentication(401, e.getMessage(), httpServletRequest.getRequestURI()));
            return false;
        }
        return true;
    }
//
//    public static void addBlackList(String token, StringRedisTemplate redisTemplate) {
//        if (!token.isEmpty()) {
//            RedisUtils.hset("blackList", token, df.format(LocalDateTime.now()), redisTemplate);
//        }
//    }
//
//    public static void deleteRedisToken(String token, StringRedisTemplate redisTemplate) {
//        if (!token.isEmpty()) {
//            RedisUtils.deleteKey(token, redisTemplate);
//        }
//    }

    public static boolean isBlackList(String token, Token entity) {
        if (!token.isEmpty()) {
            return entity.isBlackList();
        }
        return false;
    }

    public static boolean isExpiration(String expiration) {
        LocalDateTime expirationTime = LocalDateTime.parse(expiration, df);
        LocalDateTime localDateTime = LocalDateTime.now();
        return localDateTime.compareTo(expirationTime) > 0;
    }
//
//    public static boolean isValid(String refreshTime) {
//        LocalDateTime validTime = LocalDateTime.parse(refreshTime, df);
//        LocalDateTime localDateTime = LocalDateTime.now();
//        return localDateTime.compareTo(validTime) > 0;
//    }
//
//    public static boolean hasToken(String token, StringRedisTemplate redisTemplate) {
//        if (!token.isEmpty()) {
//            return RedisUtils.hasKey(token, redisTemplate);
//        }
//        return false;
//    }
//
//    public static String getExpirationByToken(String token, StringRedisTemplate redisTemplate) {
//        if (!token.isEmpty()) {
//            return RedisUtils.hget(token, "expiration", redisTemplate);
//        }
//        return null;
//    }
//
//    public static String getRefreshTimeByToken(String token, StringRedisTemplate redisTemplate) {
//        if (!token.isEmpty()) {
//            return RedisUtils.hget(token, "refreshTime", redisTemplate).toString();
//        }
//        return null;
//    }
//
//    public static String getUserNameByToken(String token, StringRedisTemplate redisTemplate) {
//        if (!token.isEmpty()) {
//            return RedisUtils.hget(token, "username", redisTemplate).toString();
//        }
//        return null;
//    }
//
//    public static String getIpByToken(String token, StringRedisTemplate redisTemplate) {
//        if (!token.isEmpty()) {
//            return RedisUtils.hget(token, "ip", redisTemplate).toString();
//        }
//        return null;
//    }

}
