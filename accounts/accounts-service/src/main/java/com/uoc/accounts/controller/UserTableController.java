package com.uoc.accounts.controller;

import com.uoc.accounts.config.security.model.SysUserDetails;
import com.uoc.accounts.config.security.token.JWTTokenUtils;
import com.uoc.accounts.domain.dto.UserCheckToken;
import com.uoc.accounts.domain.dto.UserTableDTO;
import com.uoc.accounts.entity.GroupTable;
import com.uoc.accounts.entity.PermissionTable;
import com.uoc.accounts.entity.RoleTable;
import com.uoc.accounts.entity.UserTable;
import com.uoc.accounts.service.IUserTableService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/accounts/user")
public class UserTableController {
    @Autowired
    IUserTableService service;


    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionView('user'))")
    public ResponseEntity getById(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findById(id));
    }

    @GetMapping()
    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionView('user'))")
    public ResponseEntity getById1() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getAll());
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('add')")
    public ResponseEntity update(@PathVariable int id, @RequestBody UserTableDTO userDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.update(id, userDTO));
    }

    @PostMapping("/create")
//    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionAdd('user'))")
    public ResponseEntity create(@RequestBody UserTableDTO userDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.create(userDTO));
    }

    @GetMapping("/activeUser")
    // validate: check exists, check not expired
    public ResponseEntity<?> activeUserViaEmail(@RequestParam String token) {
        // active user
        service.activeUser(token);

        return new ResponseEntity<>("Active success!", HttpStatus.OK);
    }

    @GetMapping("/checkToken")
//    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionAdd('user'))")
    public ResponseEntity checkToken(@RequestHeader String Authorization, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        SysUserDetails sysUserDetails = null;
        UserCheckToken userCheckToken = new UserCheckToken();
        if (JWTTokenUtils.checkToken(Authorization, httpServletResponse, httpServletRequest)) {
            sysUserDetails = JWTTokenUtils.parseAccessToken(service, Authorization);
            BeanUtils.copyProperties(sysUserDetails, userCheckToken);
            List<String> permissions = new ArrayList<>();
            for (GrantedAuthority grantedAuthority : sysUserDetails.getAuthorities()) {
                permissions.add(grantedAuthority.toString());
            }
            userCheckToken.setPermission(permissions);
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(userCheckToken);
    }

    @GetMapping("/permission/{id}")
    public ResponseEntity getPermission(@PathVariable int id) {
        Set<String> permissions = new HashSet<>();
        UserTable userTable = service.findById(id);

//        userTable.getPermissions().stream().map(data -> permission.add(data.getName()));

        for (PermissionTable permission: userTable.getPermissions()) {
            permissions.add(permission.getName());
        }

        for (RoleTable role: userTable.getRoles()) {
            for (PermissionTable permission: role.getPermissions()) {
                permissions.add(permission.getName());
            }
        }

        for (GroupTable group: userTable.getGroups()) {
            for (PermissionTable permission: group.getPermissions()) {
                permissions.add(permission.getName());
            }
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(permissions);
    }

    @PostMapping("/add-permission/{id}")
    public ResponseEntity addPermission(@PathVariable int id, @RequestParam int permission) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.addPermission(id, permission));
    }
}