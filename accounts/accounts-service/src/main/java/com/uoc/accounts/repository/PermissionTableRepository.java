package com.uoc.accounts.repository;

import com.uoc.accounts.entity.PermissionTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionTableRepository extends JpaRepository<PermissionTable, Integer> {
}
