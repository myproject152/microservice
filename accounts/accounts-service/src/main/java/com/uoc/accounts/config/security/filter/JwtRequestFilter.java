package com.uoc.accounts.config.security.filter;

import com.uoc.accounts.config.security.model.SysUserDetails;
import com.uoc.accounts.config.security.token.JWTConfig;
import com.uoc.accounts.config.security.token.JWTTokenUtils;
import com.uoc.accounts.service.impl.UserTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
//    @Autowired
//    private StringRedisTemplate redisTemplate;

    //    public JwtRequestFilter(AuthenticationManager authenticationManager) {
//        super(authenticationManager);
//    }
    @Autowired
    private UserTableService userEntityService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String header = httpServletRequest.getHeader(JWTConfig.tokenHeader);
        if (StringUtils.endsWithIgnoreCase(httpServletRequest.getRequestURI(), "/login") ||
                StringUtils.startsWithIgnoreCase(httpServletRequest.getRequestURI(), "/h2-console/")
                || StringUtils.endsWithIgnoreCase(httpServletRequest.getRequestURI(),"/create")
        || StringUtils.startsWithIgnoreCase(httpServletRequest.getRequestURI(), "/api/accounts/user/activeUser")) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } else {
            if (JWTTokenUtils.checkToken(header, httpServletResponse, httpServletRequest)) {

                SysUserDetails sysUserDetails = JWTTokenUtils.parseAccessToken(userEntityService, header);

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        sysUserDetails, sysUserDetails.getId(), sysUserDetails.getAuthorities());

                SecurityContextHolder.getContext().setAuthentication(authentication);

                JWTTokenUtils.updateExpiration(header);
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
        }

//        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
