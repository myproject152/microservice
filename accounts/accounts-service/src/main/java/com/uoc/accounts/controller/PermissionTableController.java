package com.uoc.accounts.controller;

import com.uoc.accounts.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/accounts/permission")
public class PermissionTableController {
    @Autowired
    IPermissionService service;

    @GetMapping()
//    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionView('user'))")
    public ResponseEntity getAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getAll());
    }

    @PostMapping("/create")
//    @PreAuthorize("hasAnyAuthority(@securityService.getPermissionView('user'))")
    public ResponseEntity getById(@RequestParam String name) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.save(name));
    }

}
