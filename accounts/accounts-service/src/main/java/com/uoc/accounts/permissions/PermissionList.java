package com.uoc.accounts.permissions;

public class PermissionList {
    public enum entity{
        ALLOWANCE_TYPE,
        ASSISTANT_TYPE,
        BIOMETRIC_DEVICE,
        BIOMETRIC_TYPE,
        BLOOD_TYPE,
        BUSINESS_TYPE,
        CONTRACT_TYPE,
        CUSTOMER_TYPE,
        DEPARTMENT_TYPE,
        DIPLOMA_TYPE,
        DISCIPLINE_TYPE,
        EMPLOYEE,
        EMP_ALLOWANCE,
        EMP_ASSISTANT,
        EMP_BIOMETRIC,
        EMP_CONTRACT,
        EMP_EXPERIENCE,
        EMP_HOBBY,
        EMP_INSURANCE,
        EMP_MANAGER,
        EMP_PROFESSION,
        EMP_ROLE,
        GENDER_TYPE,
        GRADUATE_TYPE,
        HOBBY_TYPE,
        IDENTITY_TYPE,
        INDUSTRY_TYPE,
        INSURANCE_TYPE,
        MARITAL_TYPE,
        NATIONAL_TYPE,
        OCCUPATION_TYPE,
        ORGANIZATION,
        ORG_ASSISTANT,
        ORG_LOCATION,
        ORG_MANAGER,
        ORG_ROLE,
        ORGANIZATION_TYPE,
        PARTNER,
        PARTNER_BANK,
        PARTNER_CONTACT,
        PARTNER_IDENTITY,
        PARTNER_INDUSTRY,
        PARTNER_ROLE,
        PARTNER_TYPE,
        POSITION_TYPE,
        PROFESSION_TYPE,
        RECRUIT_TYPE,
        RELATION_TYPE,
        RELIGION_TYPE,
        REWARD_TYPE,
        SEGMENT_TYPE,
        SEXUAL_TYPE,
        SOLDIER_TYPE
    }
    public enum PartnerPermission {
        PARTNER_IMPORT_FILE,
        PARTNER_VIEW_LIST_PERMISSION
    }

    public enum PermissionDefault {
        VIEW,
        ADD,
        UPDATE,
        DELETE
    }
}
