package com.uoc.accounts.service.impl;

import com.uoc.accounts.config.exceptions.AppException;
import com.uoc.accounts.config.exceptions.ErrorResponseBase;
import com.uoc.accounts.entity.PermissionTable;
import com.uoc.accounts.repository.PermissionTableRepository;
import com.uoc.accounts.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionService implements IPermissionService {
    @Autowired
    PermissionTableRepository repository;
    @Override
    public PermissionTable getById(int id) {
        if (repository.findById(id).isEmpty()){
            throw new AppException(ErrorResponseBase.NOT_FOUND);
        }
        return repository.findById(id).get();
    }

    @Override
    public PermissionTable save(String name) {
        PermissionTable permissionTable = new PermissionTable();
        permissionTable.setName(name);
        return repository.save(permissionTable);
    }

    @Override
    public List<PermissionTable> getAll() {
        return repository.findAll();
    }
}
