package com.uoc.accounts.service;

import com.uoc.accounts.domain.dto.UserTableDTO;
import com.uoc.accounts.entity.UserTable;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface IUserTableService extends UserDetailsService {
    UserTable update(int id, UserTableDTO userDTO);

    UserTable create(UserTableDTO userDTO);

    UserTable findById(int id);

    List<UserTable> getAll();

    UserTable addPermission(int id, int permission);

    UserTable findByEmail(String email);

    void activeUser(String token);
}
