package com.uoc.accounts.repository;

import com.uoc.accounts.entity.UserTable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserTableRepository extends JpaRepository<UserTable, Integer> {
    Optional<UserTable> findByUsername(String username);

    Optional<UserTable> findByEmail(String email);

    Optional<UserTable> findByUuid(String uuid);

}
