package com.uoc.accounts.service.impl;

import com.uoc.accounts.config.exceptions.AppException;
import com.uoc.accounts.entity.UserTable;
import com.uoc.accounts.service.IEmailService;
import com.uoc.accounts.service.IUserTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

@Component
public class EmailService implements IEmailService {

    @Autowired
    private IUserTableService userService;

    @Autowired
    private JavaMailSender mailSender;

    /*
     * @see
     * com.vti.service.IEmailService#sendRegistrationUserConfirm(java.lang.String)
     */
    @Override
    public void sendRegistrationUserConfirm(String email) {
        UserTable user = userService.findByEmail(email);
        String token = user.getUuid();

        String confirmationUrl = "http://localhost:8000/api/accounts/user/activeUser?token=" + token;

        String subject = "Xác Nhận Đăng Ký Account";
        String content = "<div>Bạn đã đăng ký thành công. Click vào link dưới đây để kích hoạt tài khoản</div><br/>"
                +"<p>" + confirmationUrl + "<p>"
				+"<img src='http://www.apache.org/images/asf_logo_wide.gif'>";

        sendEmail(email, subject, content);

    }

    private void sendEmail(final String recipientEmail, final String subject, final String content) {
//		SimpleMailMessage message = new SimpleMailMessage();
        try {
            MimeMessage message = mailSender.createMimeMessage();
            boolean multipart = true;
            MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

            helper.setTo(recipientEmail);
            helper.setSubject(subject);
			message.setContent(content, "text/html");

            mailSender.send(message);
        } catch (Exception e) {
            throw new AppException(e);
        }

    }

}
