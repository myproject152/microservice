drop database if exists jwt;
create database jwt;
use jwt;

drop table if exists users_permissions;
drop table if exists groups_permissions;
drop table if exists roles_permissions;

drop table if exists permission_table;
drop table if exists users_groups;
drop table if exists users_roles;

drop table if exists groups_roles;
drop table if exists user_table;
drop table if exists group_table;
drop table if exists role_table;

create table `user_table` (
                              `id` int(10) not null auto_increment,
                              `uuid` varchar(32) not null unique,
                              `create_user_id` int(10) not null,
                              `modified_user_id` int(10) not null,
                              `modified_date` date not null,
                              `create_date` date not null,
                              `username` varchar(255) not null unique,
                              `password` varchar(255) not null,
                              `first_name` varchar(255) not null,
                              `phone_number` varchar(11) not null unique,
                              `email` varchar(100) not null unique,
                              `status` int(2) not null unique,
                              primary key (`id`)
);

create table `group_table` (
                               `id` int(10) not null auto_increment,
                               `name` varchar(100) not null unique,
                               `status` int(2) not null,
                               `description` varchar(255) not null,
                               primary key (`id`)
);

create table `users_groups` (
                                `user_id` int(10) not null,
                                `group_id` int(10) not null,
                                foreign key (user_id) REFERENCES user_table(id),
                                foreign key (group_id) REFERENCES group_table(id)
);

create table `role_table` (
                              `id` int(10) not null auto_increment unique,
                              `name` varchar(100) not null unique,
                              `status` int(2) not null,
                              `description` varchar(255) not null,
                              primary key (`id`)
);

create table `users_roles` (
                               `user_id` int(10) not null,
                               `role_id` int(10) not null,
                               foreign key (user_id) REFERENCES user_table(id),
                               foreign key (role_id) REFERENCES role_table(id)
);

create table `groups_roles` (
                                `group_id` int not null,
                                `role_id` int not null,
                                foreign key (group_id) REFERENCES group_table(id),
                                foreign key (role_id) REFERENCES role_table(id)
);

create table `permission_table` (
                                    `id` int(10) not null auto_increment unique,
                                    `name` varchar(100) not null unique,
                                    primary key (`id`)
);

create table `users_permissions` (
                                  `user_id` int not null,
                                  `permissions_id` int not null,
                                  foreign key (user_id) REFERENCES user_table(id),
                                  foreign key (permissions_id) REFERENCES permission_table(id)
);

create table `groups_permissions` (
                                     `group_id` int(10) not null,
                                     `permissions_id` int(10) not null,
                                     foreign key (group_id) REFERENCES group_table(id),
                                     foreign key (permissions_id) REFERENCES permission_table(id)
);

create table `roles_permissions` (
                                    `permissions_id` int(10) not null,
                                    `role_id` int(10) not null,
                                    foreign key (role_id) REFERENCES role_table(id),
                                    foreign key (permissions_id) REFERENCES permission_table(id)
);

create table token
(
    id            bigint auto_increment
        primary key,
    expiration    datetime(6)  null,
    ip            varchar(255) null,
    is_black_list bit          null,
    refresh_time  datetime(6)  null,
    token         varchar(500) null
);

