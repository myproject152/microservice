-- user_table (pass = 123456) ---
INSERT INTO user_table (id, uuid, create_user_id, modified_user_id, modified_date, create_date, username, password, first_name, phone_number, email, status)
VALUES (1, 'cc1f3652-cf46-11ec-9d64-0242ac12', 1, 1, '2022-05-09', '2022-05-09', 'vanuoc2', '$2a$10$vYsLLIbOiKPCAvmKDnmucueEoRsFXtxLUKjuuiA8Dks0/IiApoYw6', 'vanuoc', '0377106297', 'vanuoc9xhy@gmail.com', 1);


-- permission --
INSERT INTO permission_table    (id, name)
                    VALUES      (1, 'add'),
                                (3, 'delete'),
                                (2, 'update'),
                                (4, 'view');

INSERT INTO users_permissions (user_id, permission_id)
VALUES      (1, 1),
            (1,2 ),
            (1, 3),
            (1, 4);
