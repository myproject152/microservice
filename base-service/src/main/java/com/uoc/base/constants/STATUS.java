package com.uoc.base.constants;

public enum STATUS {
    ACTIVE(1, "active"),
    DE_ACTIVE(2,"de-active"),
    PENDING(3,"pending");

    public final int status;
    public final String display;

    STATUS(int status, String display) {
        this.status = status;
        this.display = display;
    }

    public int getStatus() {
        return status;
    }

    public String getDisplay() {
        return display;
    }
}
