package com.uoc.language.controller;

import com.uoc.language.config.CardsServiceConfig;
import com.uoc.language.event.restTemplate.AccountRestTemplate;
import com.uoc.language.service.ILanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/language")
public class LanguageController {
    @Autowired
    ILanguageService service;

    @Autowired
    CardsServiceConfig cardsConfig;

    @Autowired
    AccountRestTemplate accountRestTemplate;

    @Autowired
    HttpServletRequest httpServletRequest;

//    @GetMapping("/{id}")
//    public ResponseEntity getById(@PathVariable int id) {
//        return ResponseEntity.status(HttpStatus.OK)
//                .body(service.getPermission(id));
//    }

    @GetMapping("/checkToken")
    public boolean checkToken(HttpServletRequest httpServletRequest, Principal principal) {
        System.out.println(principal.toString());
        return service.checkToken(httpServletRequest);
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('view')")
    public ResponseEntity test(HttpServletRequest httpServletRequest, Principal principal) {
        System.out.println(principal.toString());
        return ResponseEntity.status(HttpStatus.OK)
                .body(principal);
    }

    @GetMapping("/cards")
    public ResponseEntity getPropertyDetails() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(cardsConfig);
    }

    @GetMapping("/test")
    public ResponseEntity test2() {
//        System.out.println(principal.toString());
        String token = httpServletRequest.getHeader("Authorization");
        String userAgent = httpServletRequest.getHeader("User-Agent");
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountRestTemplate.checkToken(token, userAgent));
    }

}
