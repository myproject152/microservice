package com.uoc.language.config.security.filter;

import com.uoc.domain.sercurity.SysUserDetails;
import com.uoc.language.config.security.exeption.ResponseAuthentication;
import com.uoc.language.controller.LanguageController;
import com.uoc.language.service.ILanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    ILanguageService languageService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
//        try {
//            if (languageService.checkToken(httpServletRequest)){
//                filterChain.doFilter(httpServletRequest, httpServletResponse);
//            } else {
//                ResponseAuthentication.responseJson(httpServletResponse, new ResponseAuthentication(401, "Lỗi", httpServletRequest.getRequestURI()));
//            }
//        } catch (Exception e){
//            ResponseAuthentication.responseJson(httpServletResponse, new ResponseAuthentication(401, e.getMessage(), httpServletRequest.getRequestURI()));
//        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);

    }
}
