package com.uoc.language.config;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

/**
 * @author Eazy Bytes
 *
 */
@Component
@ConfigurationProperties(prefix = "cards")
@Getter
@Setter
@ToString
public class CardsServiceConfig {
//	@Value("${cards.msg}")
	 private String msg;

//	@Value("${cards.build-version}")
	 private String buildVersion;

//	@Value("${cards.mailDetails}")
//	private String mailDetails;

//	@Value("${cards.activeBranches}")
//	 private String activeBranches;

	private Map<String, String> mailDetails;
	private List<String> activeBranches;

}
