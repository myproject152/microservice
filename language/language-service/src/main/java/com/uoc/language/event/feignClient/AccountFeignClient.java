package com.uoc.language.event.feignClient;

import com.uoc.domain.sercurity.SysUserDetails;
import com.uoc.domain.sercurity.UserTable;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

@FeignClient("accounts")
@Headers({
        "Accept: application/json; charset=utf-8",
        "Content-Type: application/json" })
public interface AccountFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/api/accounts/user/permission/{id}", consumes = "application/json")
    Set<String> getPermission(@PathVariable int id);

    @RequestMapping(method = RequestMethod.GET, value = "/api/accounts/user/checkToken", consumes = "application/json")
    SysUserDetails checkToken(@RequestHeader String Authorization, @RequestHeader String UserAgent);
}
