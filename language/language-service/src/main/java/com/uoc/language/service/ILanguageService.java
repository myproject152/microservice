package com.uoc.language.service;

import javax.servlet.http.HttpServletRequest;

public interface ILanguageService {
    boolean checkToken(HttpServletRequest httpServletRequest);
}
