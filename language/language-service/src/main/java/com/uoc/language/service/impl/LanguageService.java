package com.uoc.language.service.impl;

import com.uoc.domain.sercurity.SysUserDetails;
import com.uoc.language.config.exceptions.AppException;
import com.uoc.language.config.exceptions.ErrorResponseBase;
import com.uoc.language.event.feignClient.AccountFeignClient;
import com.uoc.language.service.ILanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional(rollbackFor = Exception.class)
public class LanguageService implements ILanguageService {
    @Autowired
    AccountFeignClient accountFeignClient;

    @Override
    public boolean checkToken(HttpServletRequest httpServletRequest) {
        try {
            String token = httpServletRequest.getHeader("Authorization");
            String UserAgent = httpServletRequest.getHeader("User-Agent");
            SysUserDetails sysUserDetails = accountFeignClient.checkToken(token, UserAgent);



            if (sysUserDetails.getId()==null){
                throw new AppException(ErrorResponseBase.UNAUTHORIZED);
            }else {
                Set<GrantedAuthority> permissions = new HashSet<>();
                for (String permission : sysUserDetails.getPermission()) {
                    permissions.add(new SimpleGrantedAuthority(permission));
                }
                sysUserDetails.setAuthorities(permissions);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        sysUserDetails, sysUserDetails.getId(), sysUserDetails.getAuthorities());

                SecurityContextHolder.getContext().setAuthentication(authentication);
                return true;
            }

        }catch (Exception e){
            throw new AppException(e);
        }
    }

    private Set<GrantedAuthority> getAuthorities(Object obj){
        Set<GrantedAuthority> authorities = new HashSet<>();
        Field[] authoritiesObj = obj.getClass().getFields();
//        GrantedAuthority authority = new SimpleGrantedAuthority();

        return null;
    }
}
