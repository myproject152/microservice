package com.uoc.language.event.restTemplate;

import com.google.gson.Gson;
import com.uoc.domain.sercurity.SysUserDetails;
import com.uoc.language.config.exceptions.AppException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AccountRestTemplate {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${api.host.baseurl}")
    private String apiHost;

    public SysUserDetails checkToken(String authorization, String userAgent) {
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<SysUserDetails> result = null;
        SysUserDetails userDetails = null;

        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", authorization);
        headers.add("UserAgent", userAgent);

        HttpEntity<SysUserDetails> requestBody = new HttpEntity<>(headers);

        try {
            result = restTemplate.exchange(apiHost + "/api/accounts/user/checkToken",
                    HttpMethod.GET, requestBody, SysUserDetails.class);
            if (result.getStatusCode() == HttpStatus.OK) {
                userDetails = result.getBody();
            } else {
                System.out.println(result.getBody().toString());
            }
            return userDetails;
        } catch (Exception e) {
            String mess = e.getMessage().replace("401 : [", "")
                    .replace("]", "");
            Gson gson = new Gson();
            AppException appException = gson.fromJson(mess, AppException.class);
            throw appException;
        }

    }
}
