package com.uoc.domain.sercurity;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Token {
    private Long id;

    private String token;

    private String ip;

    private boolean isBlackList;

    private LocalDateTime refreshTime;

    private LocalDateTime expiration;

    public Token(String token, String ip, boolean isBlackList, LocalDateTime refreshTime, LocalDateTime expiration) {
        this.token = token;
        this.ip = ip;
        this.isBlackList = isBlackList;
        this.refreshTime = refreshTime;
        this.expiration = expiration;
    }
}
