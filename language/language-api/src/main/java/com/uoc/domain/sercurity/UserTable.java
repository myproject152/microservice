package com.uoc.domain.sercurity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserTable {
    private Integer id;

    private String uuid;

    private String username;

    private String firstName;

    private String password;

    private String phoneNumber;

    private String email;

    private Integer status;

    private List<String> permission;
}
