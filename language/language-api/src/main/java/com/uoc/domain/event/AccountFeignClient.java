package com.uoc.domain.event;

import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

@FeignClient("accounts")
@Headers({
        "Accept: application/json; charset=utf-8",
        "Content-Type: application/json" })
public interface AccountFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/api/accounts/user/permission/{id}", consumes = "application/json")
    Set<String> getPermission(@PathVariable int id);
}
